﻿using System;

namespace TestBuildPackage
{
    public enum TimeType
    {
        Local,
        Utc
    }
    
    public class Now
    {
        /// <summary>
        /// Gets current time
        /// </summary>
        /// <param name="type"></param>
        /// <returns>Current time</returns>
        /// <exception cref="ArgumentException"></exception>
        public DateTime Get(TimeType type, bool param = true)
        {
            switch (type)
            {
                case TimeType.Local:
                    return DateTime.Now;
                case TimeType.Utc:
                    return DateTime.UtcNow;
            }
            
            throw new ArgumentException("type");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var now = new Now();
            Console.WriteLine(now.Get(TimeType.Local, true));
            Console.WriteLine(now.Get(TimeType.Utc, false));
        }
    }
}