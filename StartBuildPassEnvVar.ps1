Param([string]$myVar )

$token = 'c8neuww0s3520vttn0io'

$headers = @{
  "Authorization" = "Bearer $token"
  "Content-type" = "application/json"
}

$body = @{
    accountName="Vitone"
    projectSlug="testrefpackage-yev2e"
    branch="master"    
    environmentVariables = @{
       my_var=$myVar 
    }    
}
$body = $body | ConvertTo-Json

Invoke-RestMethod -Uri 'https://ci.appveyor.com/api/builds' -Headers $headers  -Body $body -Method POST